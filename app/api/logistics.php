<?php
  /*
  * Api's route will point to the corresponding method for processing
  * sending response to the callback
  *
  * (c) Saptalabs Software Solutoins pvt ltd
  */

  //Including the constants.php file to get the constants
  include_once dirname(__FILE__) . '/constants.php';
	require_once dirname(__FILE__) . '/logisticsMiddleware.php';

    $DriverInfo = array();
    $DeviceInfo = array();
    $LocationInfo = array();
    $EventsInfo = array();

  // CRUD OPERATIONS FOR USER (DRIVER)

  /* *
  * URL: http://ldsservice.local/api/createdriver
  * Parameters: DriverID, FacilityID, Password, Photo(blob image data)
  * Method: POST
  * Response: Array -> error(boolean), message
  * */
  $app->post('/api/createdriver', function ($request) {
    $req = json_decode(file_get_contents('php://input'), true);
    $response = array();
    $req_driver = $req['DriverInfo'];
    $req_device = $req['DeviceInfo'];
    $req_location = $req['LocationInfo'];
  
    $DriverID = $req_driver['DriverID'];
    $FacilityID  = $req_driver['FacilityID'];
    $Password  = $req_driver['Password'];
    $Photo  = $req_driver['Photo'];

    $db = new logisticsMiddleware();
    $res = $db->createDriver($DriverID, $FacilityID, $Password, $Photo);
    if ($res == 0) {
      $response["status"] = false;
      $response["response"] = "You are successfully registered";
      echo json_encode($response);
    } else if ($res == 1) {
      $response["status"] = true;
      $response["response"] = "Oops! An error occurred while registereing";
      echo json_encode($response);
    } else if ($res == 2) {
      $response["status"] = true;
      $response["response"] = "Sorry, this driver  already existed";
      echo json_encode($response);
    }
  });

  /* *
  * URL: http://ldsservice.local/api/updatedriver
  * Parameters: DriverID, FacilityID, Photo
  * Method: PUT
  * Response: Array -> error(boolean), message
  * */
  $app->put('/api/updatedriver', function ($request) {
    $response = array();
    $DriverID = $request->getParsedBody()['DriverID'];
    $FacilityID = $request->getParsedBody()['FacilityID'];
    $Photo = $request->getParsedBody()['Photo'];

    $db = new logisticsMiddleware();
    $res = $db->updateDriver($DriverID, $FacilityID, $Photo);
    if ($res == 0) {
      $response["status"] = false;
      $response["response"] = "You are successfully updated the driver details";
      echo json_encode($response);
    } else if ($res == 1) {
      $response["status"] = true;
      $response["response"] = "Oops! An error occurred while updating";
      echo json_encode($response);
    } else if ($res == 2) {
      $response["status"] = true;
      $response["response"] = "Sorry, this driver doesn't exists";
      echo json_encode($response);
    }
  });

  /* *
  * URL: http://ldsservice.local/api/drivers
  * Parameters: none
  * Method: GET
  * Response: Array -> error(boolean), drivers array with (DriverID, FacilityID, Password, 
  * Token, Photo) n rows
  * */
  $app->get('/api/drivers', function($request) {
    
    $req = json_decode(file_get_contents('php://input'), true);

    $db = new logisticsMiddleware();
    $result = $db->getAllManifest($req['DriverID']);
    $response = array();
    $response['status'] = false;
    $response['response'] = array();

    while($row = $result->fetch_assoc()) {
      $temp = array();
      $temp['DriverID'] = $row['DriverID'];
      $temp['FacilityID'] = $row['FacilityID'];
      $temp['Password'] = $row['Password'];
      $temp['Token'] = $row['Token'];
      $temp['Photo'] = $row['Photo'];
      array_push($response['response'],$temp);
    }

    echo json_encode($response);
  });
  
  /* ========================================================================================
  *   OPERATIONS FOR LOGIN AUTHENTICATING, LOGOUT, GETTING MANIFEST DETAILS, 
  *   UPDATING MANIFEST DETAILS, EVENTS UPDATES 
  * =========================================================================================
  */

	/* *
	* URL: http://ldsservice.local/api/login
	* Parameters: Array -> driverInfo, deviceInfo, locationInfo
	* Method: POST
  * Response: Array -> error(boolean), message
	* */
  $app->post('/api/login', function($request) {

    $req = json_decode(file_get_contents('php://input'), true);
    
    $req_driver = $req['DriverInfo'];
    $req_device = $req['DeviceInfo'];
    $req_location = $req['LocationInfo'];
	
    $DriverInfo['DriverID'] = $req_driver['DriverID'];
    $DriverInfo['FacilityID'] = $req_driver['FacilityID'];
    $DriverInfo['Password'] = $req_driver['Password'];

    $DeviceInfo['DeviceID'] = $req_device['DeviceID'];
    $DeviceInfo['PhoneNum'] = $req_device['PhoneNum'];
    $DeviceInfo['PhoneIp'] = $req_device['PhoneIp'];
    $DeviceInfo['AppVersion'] = $req_device['AppVersion'];
    $BarcodeMaxLength = barcode_maxLength;

    $LocationInfo['Longitude'] = $req_location['Longitude'];
    $LocationInfo['Latitude'] = $req_location['Latitude'];
    $LocationInfo['GPSPrecision'] = $req_location['GPSPrecision'];
    $LocationInfo['GPSFixDateTime'] = $req_location['GPSFixDateTime']; 
    $GPSInterval = gps_interval;

    $db = new logisticsMiddleware();
    $response = array();
    if ($db->driverLogin($DriverInfo['DriverID'], $DriverInfo['Password'], $req['SecurityGuid'])) {
      $device = $db->driverDeviceUpdate($DriverInfo['DriverID'], 
                                          $DeviceInfo['DeviceID'], 
                                          $DeviceInfo['PhoneNum'], 
                                          $DeviceInfo['PhoneIp'], 
                                          $DeviceInfo['AppVersion'], 
                                          $BarcodeMaxLength);
      if($device == 0 || $device == 2) {
        $location = $db->driverLocationUpdate($DriverInfo['DriverID'], 
                                                $LocationInfo['Latitude'], 
                                                $LocationInfo['Longitude'], 
                                                $LocationInfo['GPSPrecision'], 
                                                $LocationInfo['GPSFixDateTime'], 
                                                $GPSInterval);
        if($location == 0 || $location == 2) {
          $driver = $db->getLoginDetails($DriverInfo['DriverID']);
          $res = $db->getAllContainerDetails();
          $response['status'] = 200;
          $response['message'] = "Driver authenticated";
          $response['responseJSON'] = array();
          $response['responseJSON']['Conatiners'] = array();

          while($row = $res->fetch_assoc()) {
            $containerDetails = array();
            $containerDetails['Code'] = $row['Code'];
            $containerDetails['Label'] = $row['Label'];
            array_push($response['responseJSON']['Conatiners'],$containerDetails);
          }
          $response['responseJSON']['DriverName'] = $driver['DriverID'];
          $response['responseJSON']['Token'] = $driver['Token'];
          $response['responseJSON']['GPSInterval'] = $driver['GPSInterval'];
          $response['responseJSON']['BarcodeMaxLength'] = $driver['BarcodeMaxLength'];
          $response['responseJSON']['currentDriverId'] = $driver['DriverID'];
          $response['responseJSON']['Error'] = false;
          $response['responseJSON']['ErrorMessage'] = "";
          $response['responseJSON']['isSuccessful'] = true;
          $response['responseJSON']['statusCode'] = 200;
          $response['responseJSON']['statusReason'] = "OK";
          $response['responseJSON']['FacilityCode'] = "DULL";

          echo json_encode($response);
        } else if($location == 1) {
          $response['status'] = true;
          $response['response'] = "Error occured while inserting location details.";

          echo json_encode($response);
        } else if($location == 3) {
          $response['status'] = true;
          $response['response'] = "Error occured while updating location details.";

          echo json_encode($response);
        }
      } else if($device == 1) {
        $response['status'] = true;
        $response['response'] = "Error occured while inserting device details.";

        echo json_encode($response);
      } else if($device == 3) {
        $response['status'] = true;
        $response['response'] = "Error occured while updating device details.";

        echo json_encode($response);
      }
    } else {
      $response['status'] = true;
      $response['responseJSON']['Error'] = true;
      $response['responseJSON']['ErrorMessage'] = "Invalid username or password";
      $response['responseJSON']['isSuccessful'] = false;
      $response['responseJSON']['statusCode'] = 404;
      echo json_encode($response);
    }
  });

  /* *
  * URL: http://ldsservice.local/api/logout
  * Parameters: Array -> driverInfo, deviceInfo, locationInfo , BarcodeMaxLength,  $GPSInterval
  * Method: POST
  * Response: Array -> error(boolean), message
  * */
  $app->post('/api/logout', function($request) {

    $req = json_decode(file_get_contents('php://input'), true);
   
    $req_driver = $req['DriverInfo'];
    $req_device = $req['DeviceInfo'];
    $req_location = $req['LocationInfo'];
  
    $DriverInfo['DriverID'] = $req_driver['DriverID'];
    $DriverInfo['FacilityID'] = $req_driver['FacilityID'];
    $DriverInfo['Password'] = $req_driver['Password'];

    $DeviceInfo['DeviceID'] = $req_device['DeviceID'];
    $DeviceInfo['PhoneNum'] = $req_device['PhoneNum'];
    $DeviceInfo['PhoneIp'] = $req_device['PhoneIp'];
    $DeviceInfo['AppVersion'] = $req_device['AppVersion'];
    $BarcodeMaxLength = barcode_maxLength;

    $LocationInfo['Longitude'] = $req_location['Longitude'];
    $LocationInfo['Latitude'] = $req_location['Latitude'];
    $LocationInfo['GPSPrecision'] = $req_location['GPSPrecision'];
    $LocationInfo['GPSFixDateTime'] = $req_location['GPSFixDateTime']; 
    $GPSInterval = gps_interval;

    $response = array();
    $response['status'] = 200;
    $response['errorMsg'] = "OK";
    $response['responseJSON'] = array();
    $response['responseJSON']['errors'] = array(); // If any error occurs 
    $response['responseJSON']['Error'] = false;
    $response['responseJSON']['ErrorMessage'] = "";
    $response['responseJSON']['isSuccessful'] = true;
    $response['responseJSON']['statusCode'] = 200;
    $response['responseJSON']['statusReason'] = "OK";
    
    echo json_encode($response);
  });

  /* *
  * URL: http://ldsservice.local/api/customerconfig
  * Parameters: Array -> driverInfo, deviceInfo, locationInfo ,SecurityGuid, EventISODateTime, CustomerID
  * Method: POST
  * Response:  Attempt Array, DropLocationArray, Questionaire Array and Success/Error Message
  * */
  $app->post('/api/customerconfig', function($request) {

    $req = json_decode(file_get_contents('php://input'), true);
    
    $req_driver = $req['DriverInfo'];
    $req_device = $req['DeviceInfo'];
    $req_location = $req['LocationInfo'];
  
    $DriverInfo['DriverID'] = $req_driver['DriverID'];
    $DriverInfo['FacilityID'] = $req_driver['FacilityID'];

    $DeviceInfo['DeviceID'] = $req_device['DeviceID'];
    $DeviceInfo['PhoneNum'] = $req_device['PhoneNum'];
    $DeviceInfo['PhoneIp'] = $req_device['PhoneIp'];
    $DeviceInfo['AppVersion'] = $req_device['AppVersion'];

    $LocationInfo['Longitude'] = $req_location['Longitude'];
    $LocationInfo['Latitude'] = $req_location['Latitude'];
    $LocationInfo['GPSPrecision'] = $req_location['GPSPrecision'];
    $LocationInfo['GPSFixDateTime'] = $req_location['GPSFixDateTime']; 
   
    $SecurityGuid = $req['SecurityGuid'];
    $EventISODateTime = $req['EventISODateTime'];
    $response = array();
    $db = new logisticsMiddleware();
    $response['status'] = 200;
    $response['errorMsg'] = "OK";
    $response['responseJSON'] = array();
    $response['responseJSON']['isSuccessful'] = true;
    $response['responseJSON']['adapterResponse'] = array();
    foreach($req['CustomerId'] as $value) {
      $adapterRes = array();
      $adapterRes['serverResponse'] = array();
      $adapterRes['_custId'] = $value;
      $adapterRes['serverResponse']['isSuccessful'] = true;
      $adapterRes['serverResponse']['statusReason'] = "OK";
      $adapterRes['serverResponse']['statusCode'] = 200;
      $adapterRes['serverResponse']['customerId'] = $value;
      $adapterRes['serverResponse']['Attempts'] = array();
      $adapterRes['serverResponse']['DropLocations'] = array();
      $adapterRes['serverResponse']['Questionnaire'] = array();

      $attempts = $db->customerAttempts();
      while($row = $attempts->fetch_assoc()){
        $attemptResp = array(); 
        $attemptResp['Code'] = $row['Code'];
        $attemptResp['Label'] = $row['Label'];
        $attemptResp['Sort'] = $row['Sort'];
        array_push($adapterRes['serverResponse']['Attempts'],$attemptResp);
      }

      $droplocations = $db->customerDroplocations();
      while($row = $droplocations->fetch_assoc()){
        $dropLocResp = array(); 
        $dropLocResp['Code'] = $row['Code'];
        $dropLocResp['Label'] = $row['Label'];
        $dropLocResp['Sort'] = $row['Sort'];
        if($row['SignatureRequired'] === 0) {
          $dropLocResp['SignatureRequired'] = false;
        } else {
          $dropLocResp['SignatureRequired'] = true;
        }
        array_push($adapterRes['serverResponse']['DropLocations'],$dropLocResp);
      }

      if($db->customerConfig($value)['Questionnaire']) {
        $questionaire = $db->customerQuestionnaire();
        while($row = $questionaire->fetch_assoc()){
          $questionaireResp = array(); 
          $questionaireResp['Label'] = $row['Label'];
          $questionaireResp['Name'] = $row['Name'];
          $questionaireResp['Type'] = $row['Type'];
          array_push($adapterRes['serverResponse']['Questionnaire'],$questionaireResp);
        }
      }

      array_push($response['responseJSON']['adapterResponse'],$adapterRes);
    }

    echo json_encode($response);      
  });
	
	/* 
 	* URL: http://ldsservice.local/api/manifest
 	* Parameters: DeviceInfo, DriverInfo, LocationInfo, GPSInterval, BarcodeMaxLength
 	* Method: POST
	* Response: Array -> error(boolean), manifestitems array n rows
 	*/
	$app->post('/api/manifest', function($request) {

    $req = json_decode(file_get_contents('php://input'), true);
    $DriverInfo = array();
    $DeviceInfo = array();
    $LocationInfo = array();
	
    $req_driver = $req['DriverInfo'];
    $req_device = $req['DeviceInfo'];
    $req_location = $req['LocationInfo'];
	
    $DriverInfo['DriverID'] = $req_driver['DriverID'];
    $DriverInfo['FacilityID'] = $req_driver['FacilityID'];

    $DeviceInfo['DeviceID'] = $req_device['DeviceID'];
    $DeviceInfo['PhoneNum'] = $req_device['PhoneNum'];
    $DeviceInfo['PhoneIp'] = $req_device['PhoneIp'];
    $DeviceInfo['AppVersion'] = $req_device['AppVersion'];
    $BarcodeMaxLength = 32;

    $LocationInfo['Longitude'] = $req_location['Longitude'];
    $LocationInfo['Latitude'] = $req_location['Latitude'];
    $LocationInfo['GPSPrecision'] = $req_location['GPSPrecision'];
    $LocationInfo['GPSFixDateTime'] = $req_location['GPSFixDateTime']; 
    $GPSInterval = 180;
	
    $db = new logisticsMiddleware();
    $result = $db->getAllManifest($DriverInfo['DriverID']);
    $response = array();
    $response['status'] = true;
    $response['responseJSON'] = array();
    $response['responseJSON']['isSuccessful'] = true;
    $response['responseJSON']['statusCode'] = 200;
    $response['responseJSON']['statusReason'] = "OK";
    $response['responseJSON']['Pieces'] = array();
    while($row = $result->fetch_assoc()) {
      $manifestResp = array();
      $manifestResp['Barcode'] = $row['Barcode']; 
  	  $manifestResp['PieceKey'] = $row['PieceKey'];
  	  $manifestResp['CustomerID'] = $row['CustomerID'];
  	  $manifestResp['SignatureRequirement'] = $row['SignatureRequirement'];
  	  $manifestResp['SignatureWaiveable'] = $row['SignatureWaiveable'];
  	  $manifestResp['HandleWithCare'] = $row['HandleWithCare'];
  	  $manifestResp['ContainerType'] = $row['ContainerType'];
  	  $manifestResp['Description'] = $row['Description'];
  	  $manifestResp['Weight'] = $row['Weight'];
  	  $manifestResp['WeightUnit'] = $row['WeightUnit'];
  	  $manifestResp['Width'] = $row['Width'];
  	  $manifestResp['Length'] = $row['Length'];
  	  $manifestResp['Height'] = $row['Height'];
  	  $manifestResp['DimensionUnit'] = $row['DimensionUnit'];
  	  $manifestResp['Attributes'] = $row['Attributes'];

      if($row['Destination'] && !$row['Origin']) {
        $manifestResp['Destination'] = array();
    	  $manifestResp['Destination']['LocationType'] = $row['LocationType']; 
    	  $manifestResp['Destination']['Contact'] = $row['Contact'];
    	  $manifestResp['Destination']['Organization'] = $row['Organization'];
    	  $manifestResp['Destination']['Address'] = $row['Address'];
    	  $manifestResp['Destination']['Address2'] = $row['Address2'];
    	  $manifestResp['Destination']['City'] = $row['City'];
    	  $manifestResp['Destination']['State'] = $row['State'];
    	  $manifestResp['Destination']['PostalCode'] = $row['PostalCode'];
    	  $manifestResp['Destination']['Country'] = $row['Country'];
    	  $manifestResp['Destination']['Phone'] = $row['Phone'];
    	  $manifestResp['Destination']['PhoneExtension'] = $row['PhoneExtension'];
    	  $manifestResp['Destination']['UTCExpectedDeliveryBy'] = $row['UTCExpectedDeliveryBy'];
    	  $manifestResp['Destination']['DeliveryRoute'] = $row['DeliveryRoute'];
    	  $manifestResp['Destination']['DeliveryRouteSequence'] = $row['DeliveryRouteSequence'];
    	  $manifestResp['Destination']['FacilityCode'] = $row['FacilityCode'];
    	  $manifestResp['Destination']['Instruction'] = $row['Instruction'];
    	  $manifestResp['Destination']['AddressQuality'] = $row['AddressQuality'];
    	  $manifestResp['Destination']['StopIdentifier'] = $row['StopIdentifier'];

        array_push($response['responseJSON']['Pieces'],$manifestResp);
      } else if(!$row['Destination'] && $row['Origin']) {
        $manifestResp['Origin'] = array();
        $manifestResp['Origin']['LocationType'] = $row['LocationType']; 
        $manifestResp['Origin']['Contact'] = $row['Contact'];
        $manifestResp['Origin']['Organization'] = $row['Organization'];
        $manifestResp['Origin']['Address'] = $row['Address'];
        $manifestResp['Origin']['Address2'] = $row['Address2'];
        $manifestResp['Origin']['City'] = $row['City'];
        $manifestResp['Origin']['State'] = $row['State'];
        $manifestResp['Origin']['PostalCode'] = $row['PostalCode'];
        $manifestResp['Origin']['Country'] = $row['Country'];
        $manifestResp['Origin']['Phone'] = $row['Phone'];
        $manifestResp['Origin']['PhoneExtension'] = $row['PhoneExtension'];
        $manifestResp['Origin']['UTCExpectedDeliveryBy'] = $row['UTCExpectedDeliveryBy'];
        $manifestResp['Origin']['DeliveryRoute'] = $row['DeliveryRoute'];
        $manifestResp['Origin']['DeliveryRouteSequence'] = $row['DeliveryRouteSequence'];
        $manifestResp['Origin']['FacilityCode'] = $row['FacilityCode'];
        $manifestResp['Origin']['Instruction'] = $row['Instruction'];
        $manifestResp['Origin']['AddressQuality'] = $row['AddressQuality'];
        $manifestResp['Origin']['StopIdentifier'] = $row['StopIdentifier'];

        array_push($response['responseJSON']['Pieces'],$manifestResp);
      } else if($row['Destination'] && $row['Origin']){
        $manifestResp['Destination'] = array();
        $manifestResp['Destination']['LocationType'] = $row['LocationType']; 
        $manifestResp['Destination']['Contact'] = $row['Contact'];
        $manifestResp['Destination']['Organization'] = $row['Organization'];
        $manifestResp['Destination']['Address'] = $row['Address'];
        $manifestResp['Destination']['Address2'] = $row['Address2'];
        $manifestResp['Destination']['City'] = $row['City'];
        $manifestResp['Destination']['State'] = $row['State'];
        $manifestResp['Destination']['PostalCode'] = $row['PostalCode'];
        $manifestResp['Destination']['Country'] = $row['Country'];
        $manifestResp['Destination']['Phone'] = $row['Phone'];
        $manifestResp['Destination']['PhoneExtension'] = $row['PhoneExtension'];
        $manifestResp['Destination']['UTCExpectedDeliveryBy'] = $row['UTCExpectedDeliveryBy'];
        $manifestResp['Destination']['DeliveryRoute'] = $row['DeliveryRoute'];
        $manifestResp['Destination']['DeliveryRouteSequence'] = $row['DeliveryRouteSequence'];
        $manifestResp['Destination']['FacilityCode'] = $row['FacilityCode'];
        $manifestResp['Destination']['Instruction'] = $row['Instruction'];
        $manifestResp['Destination']['AddressQuality'] = $row['AddressQuality'];
        $manifestResp['Destination']['StopIdentifier'] = $row['StopIdentifier'];

        $manifestResp['Origin'] = array();
        $manifestResp['Origin']['LocationType'] = $row['LocationType']; 
        $manifestResp['Origin']['Contact'] = $row['Contact'];
        $manifestResp['Origin']['Organization'] = $row['Organization'];
        $manifestResp['Origin']['Address'] = $row['Address'];
        $manifestResp['Origin']['Address2'] = $row['Address2'];
        $manifestResp['Origin']['City'] = $row['City'];
        $manifestResp['Origin']['State'] = $row['State'];
        $manifestResp['Origin']['PostalCode'] = $row['PostalCode'];
        $manifestResp['Origin']['Country'] = $row['Country'];
        $manifestResp['Origin']['Phone'] = $row['Phone'];
        $manifestResp['Origin']['PhoneExtension'] = $row['PhoneExtension'];
        $manifestResp['Origin']['UTCExpectedDeliveryBy'] = $row['UTCExpectedDeliveryBy'];
        $manifestResp['Origin']['DeliveryRoute'] = $row['DeliveryRoute'];
        $manifestResp['Origin']['DeliveryRouteSequence'] = $row['DeliveryRouteSequence'];
        $manifestResp['Origin']['FacilityCode'] = $row['FacilityCode'];
        $manifestResp['Origin']['Instruction'] = $row['Instruction'];
        $manifestResp['Origin']['AddressQuality'] = $row['AddressQuality'];
        $manifestResp['Origin']['StopIdentifier'] = $row['StopIdentifier'];

        array_push($response['responseJSON']['Pieces'],$manifestResp);
      }
    }
    echo json_encode($response);
	});

  /* 
  * URL: http://ldsservice.local/api/manifestdetails
  * Parameters: Driverinfo, Deviceinfo ,Locationinfo, Barcode, SecurityGUID
  * Method: POST
  * Response: Array -> error(boolean), manifestitems Details array
  */
  $app->post('/api/manifestdetails', function($request) {
    $req = json_decode(file_get_contents('php://input'), true);
  
    $req_driver = $req['DriverInfo'];
    $req_device = $req['DeviceInfo'];
    $req_location = $req['LocationInfo'];
  
    $DriverInfo['DriverID'] = $req_driver['DriverID'];
    $DriverInfo['FacilityID'] = $req_driver['FacilityID'];

    $DeviceInfo['DeviceID'] = $req_device['DeviceID'];
    $DeviceInfo['PhoneNum'] = $req_device['PhoneNum'];
    $DeviceInfo['PhoneIp'] = $req_device['PhoneIp'];
    $DeviceInfo['AppVersion'] = $req_device['AppVersion'];

    $LocationInfo['Longitude'] = $req_location['Longitude'];
    $LocationInfo['Latitude'] = $req_location['Latitude'];
    $LocationInfo['GPSPrecision'] = $req_location['GPSPrecision'];
    $LocationInfo['GPSFixDateTime'] = $req_location['GPSFixDateTime'];

    $Barcode = $req['BarCode'];
    $EventISODateTime = $req['EventISODateTime'];
    $SecurityGuid = $req['SecurityGuid'];
  
    $db = new logisticsMiddleware();
    $result = $db->getAllManifestDetalis($Barcode);
    $response = array();
    $response['status'] = true;
    $response['responseJSON'] = array();
    $response['responseJSON']['isSuccessful'] = true;
    $response['responseJSON']['statusCode'] = 200;
    $response['responseJSON']['statusReason'] = "OK";
    $response['responseJSON']['Pieces'] = array();
    while($row = $result->fetch_assoc()){
      $manifestDetResp = array();
      $manifestDetResp['Barcode'] = $row['Barcode']; 
      $manifestDetResp['PieceKey'] = $row['PieceKey'];
      $manifestDetResp['CustomerID'] = $row['CustomerID'];
      $manifestDetResp['SignatureRequirement'] = $row['SignatureRequirement'];
      $manifestDetResp['SignatureWaiveable'] = $row['SignatureWaiveable'];
      $manifestDetResp['HandleWithCare'] = $row['HandleWithCare'];
      $manifestDetResp['ContainerType'] = $row['ContainerType'];
      $manifestDetResp['Description'] = $row['Description'];
      $manifestDetResp['Weight'] = $row['Weight'];
      $manifestDetResp['WeightUnit'] = $row['WeightUnit'];
      $manifestDetResp['Width'] = $row['Width'];
      $manifestDetResp['Length'] = $row['Length'];
      $manifestDetResp['Height'] = $row['Height'];
      $manifestDetResp['DimensionUnit'] = $row['DimensionUnit'];
      $manifestDetResp['Attributes'] = $row['Attributes'];

      if($row['Destination'] && !$row['Origin']){
        $manifestDetResp['Destination'] = array();
        $manifestDetResp['Destination']['LocationType'] = $row['LocationType']; 
        $manifestDetResp['Destination']['Contact'] = $row['Contact'];
        $manifestDetResp['Destination']['Organization'] = $row['Organization'];
        $manifestDetResp['Destination']['Address'] = $row['Address'];
        $manifestDetResp['Destination']['Address2'] = $row['Address2'];
        $manifestDetResp['Destination']['City'] = $row['City'];
        $manifestDetResp['Destination']['State'] = $row['State'];
        $manifestDetResp['Destination']['PostalCode'] = $row['PostalCode'];
        $manifestDetResp['Destination']['Country'] = $row['Country'];
        $manifestDetResp['Destination']['Phone'] = $row['Phone'];
        $manifestDetResp['Destination']['PhoneExtension'] = $row['PhoneExtension'];
        $manifestDetResp['Destination']['UTCExpectedDeliveryBy'] = $row['UTCExpectedDeliveryBy'];
        $manifestDetResp['Destination']['DeliveryRoute'] = $row['DeliveryRoute'];
        $manifestDetResp['Destination']['DeliveryRouteSequence'] = $row['DeliveryRouteSequence'];
        $manifestDetResp['Destination']['FacilityCode'] = $row['FacilityCode'];
        $manifestDetResp['Destination']['Instruction'] = $row['Instruction'];
        $manifestDetResp['Destination']['AddressQuality'] = $row['AddressQuality'];
        $manifestDetResp['Destination']['StopIdentifier'] = $row['StopIdentifier'];
        array_push($response['responseJSON']['Pieces'],$manifestDetResp);
      } else if(!$row['Destination'] && $row['Origin']){
        $manifestDetResp['Origin'] = array();
        $manifestDetResp['Origin']['LocationType'] = $row['LocationType']; 
        $manifestDetResp['Origin']['Contact'] = $row['Contact'];
        $manifestDetResp['Origin']['Organization'] = $row['Organization'];
        $manifestDetResp['Origin']['Address'] = $row['Address'];
        $manifestDetResp['Origin']['Address2'] = $row['Address2'];
        $manifestDetResp['Origin']['City'] = $row['City'];
        $manifestDetResp['Origin']['State'] = $row['State'];
        $manifestDetResp['Origin']['PostalCode'] = $row['PostalCode'];
        $manifestDetResp['Origin']['Country'] = $row['Country'];
        $manifestDetResp['Origin']['Phone'] = $row['Phone'];
        $manifestDetResp['Origin']['PhoneExtension'] = $row['PhoneExtension'];
        $manifestDetResp['Origin']['UTCExpectedDeliveryBy'] = $row['UTCExpectedDeliveryBy'];
        $manifestDetResp['Origin']['DeliveryRoute'] = $row['DeliveryRoute'];
        $manifestDetResp['Origin']['DeliveryRouteSequence'] = $row['DeliveryRouteSequence'];
        $manifestDetResp['Origin']['FacilityCode'] = $row['FacilityCode'];
        $manifestDetResp['Origin']['Instruction'] = $row['Instruction'];
        $manifestDetResp['Origin']['AddressQuality'] = $row['AddressQuality'];
        $manifestDetResp['Origin']['StopIdentifier'] = $row['StopIdentifier'];
        array_push($response['responseJSON']['Pieces'],$manifestDetResp);
      } else if($row['Destination'] && $row['Origin']){
        $manifestDetResp['Destination'] = array();
        $manifestDetResp['Destination']['LocationType'] = $row['LocationType']; 
        $manifestDetResp['Destination']['Contact'] = $row['Contact'];
        $manifestDetResp['Destination']['Organization'] = $row['Organization'];
        $manifestDetResp['Destination']['Address'] = $row['Address'];
        $manifestDetResp['Destination']['Address2'] = $row['Address2'];
        $manifestDetResp['Destination']['City'] = $row['City'];
        $manifestDetResp['Destination']['State'] = $row['State'];
        $manifestDetResp['Destination']['PostalCode'] = $row['PostalCode'];
        $manifestDetResp['Destination']['Country'] = $row['Country'];
        $manifestDetResp['Destination']['Phone'] = $row['Phone'];
        $manifestDetResp['Destination']['PhoneExtension'] = $row['PhoneExtension'];
        $manifestDetResp['Destination']['UTCExpectedDeliveryBy'] = $row['UTCExpectedDeliveryBy'];
        $manifestDetResp['Destination']['DeliveryRoute'] = $row['DeliveryRoute'];
        $manifestDetResp['Destination']['DeliveryRouteSequence'] = $row['DeliveryRouteSequence'];
        $manifestDetResp['Destination']['FacilityCode'] = $row['FacilityCode'];
        $manifestDetResp['Destination']['Instruction'] = $row['Instruction'];
        $manifestDetResp['Destination']['AddressQuality'] = $row['AddressQuality'];
        $manifestDetResp['Destination']['StopIdentifier'] = $row['StopIdentifier'];

        $manifestDetResp['Origin'] = array();
        $manifestDetResp['Origin']['LocationType'] = $row['LocationType']; 
        $manifestDetResp['Origin']['Contact'] = $row['Contact'];
        $manifestDetResp['Origin']['Organization'] = $row['Organization'];
        $manifestDetResp['Origin']['Address'] = $row['Address'];
        $manifestDetResp['Origin']['Address2'] = $row['Address2'];
        $manifestDetResp['Origin']['City'] = $row['City'];
        $manifestDetResp['Origin']['State'] = $row['State'];
        $manifestDetResp['Origin']['PostalCode'] = $row['PostalCode'];
        $manifestDetResp['Origin']['Country'] = $row['Country'];
        $manifestDetResp['Origin']['Phone'] = $row['Phone'];
        $manifestDetResp['Origin']['PhoneExtension'] = $row['PhoneExtension'];
        $manifestDetResp['Origin']['UTCExpectedDeliveryBy'] = $row['UTCExpectedDeliveryBy'];
        $manifestDetResp['Origin']['DeliveryRoute'] = $row['DeliveryRoute'];
        $manifestDetResp['Origin']['DeliveryRouteSequence'] = $row['DeliveryRouteSequence'];
        $manifestDetResp['Origin']['FacilityCode'] = $row['FacilityCode'];
        $manifestDetResp['Origin']['Instruction'] = $row['Instruction'];
        $manifestDetResp['Origin']['AddressQuality'] = $row['AddressQuality'];
        $manifestDetResp['Origin']['StopIdentifier'] = $row['StopIdentifier'];
        array_push($response['responseJSON']['Pieces'],$manifestDetResp);
      }
    }
    echo json_encode($response);
  });

  /* * * URL: http://ldsservice.local/api/events
  * Parameters: Driverinfo, Deviceinfo,Locationinfo and Events
  * Method: POST
  * Response: Array -> error(boolean), message 
  * */
  $app->post('/api/events', function($request) {
    $req = json_decode(file_get_contents('php://input'), true);
    
    $req_driver = $req['DriverInfo'];
    $req_device = $req['DeviceInfo'];
    $req_events = $req['Events'];
    $req_location = $req_events[0]['LocationInfo'];

    $DriverInfo['DriverID'] = $req_driver['DriverID'];
    $DriverInfo['FacilityID'] = $req_driver['FacilityID'];
   
    $DeviceInfo['DeviceID'] = $req_device['DeviceID'];
    $DeviceInfo['PhoneNum'] = $req_device['PhoneNum'];
    $DeviceInfo['PhoneIp'] = $req_device['PhoneIp'];
    $DeviceInfo['AppVersion'] = $req_device['AppVersion'];
    
    $LocationInfo['Longitude'] = $req_location['Longitude'];
    $LocationInfo['Latitude'] = $req_location['Latitude'];
    $LocationInfo['GPSFixDateTime'] = $req_location['GPSFixDateTime']; 
    $EventsInfo['Barcode'] = $req_events[0]['BarCode'];
    $EventsInfo['CustomerID'] = $req_events[0]['CustomerID'];
    $EventsInfo['DoorTag'] = $req_events[0]['DoorTag'];

    if(isset($req_events[0]['EventActionValue'])) {
      $EventsInfo['EventActionValue'] = $req_events[0]['EventActionValue'];
    }else {
      $EventsInfo['EventActionValue'] = "";
    }
    $EventsInfo['EventISODateTime'] = $req_events[0]['EventISODateTime'];
    $EventsInfo['EventModifier'] = $req_events[0]['EventModifier'];
    $EventsInfo['EventType'] = $req_events[0]['EventType'];
    $EventsInfo['GroupGUID'] = $req_events[0]['GroupingGuid'];
    $EventsInfo['Location'] = $req_events[0]['Location'];
    $EventsInfo['PieceKey'] = $req_events[0]['PieceKey'];
    $EventsInfo['SecurityGUID'] = $req_events[0]['SecurityGuid'];
    $EventsInfo['SignatureWKT'] = $req_events[0]['SignatureWKT'];
    $EventsInfo['TypedNameSignature'] = $req_events[0]['TypedNameSignature'];
    $EventsInfo['EventDateTime'] = $req_events[0]['_eventDateTime'];
    $EventsInfo['EventID'] = $req_events[0]['_eventID'];

    $db = new logisticsMiddleware();

    $res = $db->insertEventsInfo($DriverInfo['DriverID'], $EventsInfo['Barcode'], 
                                  $EventsInfo['CustomerID'], $EventsInfo['DoorTag'], 
                                  $EventsInfo['EventActionValue'], $EventsInfo['EventISODateTime'], 
                                  $EventsInfo['EventModifier'], $EventsInfo['EventType'], 
                                  $EventsInfo['GroupGUID'], $EventsInfo['Location'], 
                                  $EventsInfo['PieceKey'], $EventsInfo['SecurityGUID'], 
                                  $EventsInfo['SignatureWKT'], $EventsInfo['TypedNameSignature'], 
                                  $EventsInfo['EventDateTime'], $EventsInfo['EventID'], 
                                  $LocationInfo['Latitude'], $LocationInfo['Longitude'], 
                                  $LocationInfo['GPSFixDateTime'], $DeviceInfo['DeviceID'], 
                                  $DeviceInfo['PhoneIp'], $DeviceInfo['PhoneNum'], 
                                  $DriverInfo['FacilityID']);
    
    if ($res == 1) {
      $id = $_GET['jsonID'];
      $response = array();
      $response['status'] = true;
      $response['errorMsg'] = "OK";
      $response['responseJSON'] = array();
      $response['responseJSON']['isSuccessful'] = true;
      $response['responseJSON']['adapterResponse'] = array();
      $eventsResp = array();
      $eventsResp['_eventID'] = $EventsInfo['EventID']; 
      $eventsResp['_jsonID'] = $id; 
      $eventsResp['serverResponse'] = array();
      $eventsResp['serverResponse']['isSuccessful'] = true;
      $eventsResp['serverResponse']['statusReason'] = "OK";
      $eventsResp['serverResponse']['statusCode'] = 200;
      $eventsResp['serverResponse']['errors'] = array();
      array_push($response['responseJSON']['adapterResponse'],$eventsResp);

      echo json_encode($response);
    } else if ($res == 2) {
      $response["status"] = true;
      $response['responseJSON'] = array();
      $response['responseJSON']['isSuccessful'] = false;
      $response['responseJSON']['statusReason'] = "Oops! An error occurred ";

      echo json_encode($response);
    }
  });
?>