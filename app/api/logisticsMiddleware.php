<?php
 /*
  * LogisticsMiddleware is the middleware interface to api's interaction with 
  * database for sending corresponding response
  *
  * (c) Saptalabs Software Solutoins pvt ltd
*/

  class logisticsMiddleware
  {
  	private $con;
    
    function __construct()
    {
      require_once dirname(__FILE__) . '/dbConnect.php';
      $db = new dbconnect();
      $this->con = $db->connect();
    }

  	//Method to let a driver log in
    public function driverLogin($DriverID, $Password, $Security) {
      $pass = md5($Password);
      if($this->generateToken($DriverID, md5($Security)) == 0) {
        $stmt = $this->con->prepare("SELECT * FROM `lds_driver_info` WHERE `DriverID` = ? AND `Password` = ?");
        $stmt->bind_param("ss", $DriverID, $pass);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
      } else {
        return false;
      }
    }

    //Method to generate a unique token every time for login
    private function generateToken($DriverID, $key) {
      if ($this->isDriverExists($DriverID)) {
        $stmt = $this->con->prepare("UPDATE `lds_driver_info` SET `Token` = ? WHERE `DriverID` = ?");
        $stmt->bind_param("ss", $key, $DriverID);
        $result = $stmt->execute();
        $stmt->close();
        if($result) {
          return 0;
        } else {
          return 1;
        }
      } else {
        return 2;
      }
    }

    //Method to get driver details
    public function getDriver($username) {
      $stmt = $this->con->prepare("SELECT * FROM lds_driver_info WHERE DriverID = ?");
      $stmt->bind_param("s",$username);
      $stmt->execute();
      $driver = $stmt->get_result()->fetch_assoc();
      $stmt->close();
      return $driver;
    }

    //Method to get driver details
    public function getLoginDetails($username) {
      $stmt = $this->con->prepare("SELECT A.DriverID, A.Token, A.Photo, B.GPSInterval, C.BarcodeMaxLength FROM lds_driver_info A JOIN lds_location_info B ON B.DriverID=A.DriverID JOIN lds_device_info C ON C.DriverID=A.DriverID WHERE A.DriverID = ?");
      $stmt->bind_param("s",$username);
      $stmt->execute();
      $driver = $stmt->get_result()->fetch_assoc();
      $stmt->close();
      return $driver;
    }

    //Method to register a new driver
    public function createDriver($DriverID,$FacilityID,$Password,$Photo) {
      if (!$this->isDriverExists($DriverID)) {
        $pass = md5($Password);
        $Token = $this->generateGUID();
        $stmt = $this->con->prepare("INSERT INTO `lds_driver_info` (`DriverID`, `FacilityID`, `Password`, `Token`, `Photo`) VALUES (?,?,?,?,?)");
        $stmt->bind_param("ssssb", $DriverID, $FacilityID, $pass, $Token, $Photo);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
          return 0;
        } else {
          return 1;
        }
      } else {
        return 2;
      }
    }

    //Method to update a driver details
    public function updateDriver($DriverID, $FacilityID, $Photo) {
      if ($this->isDriverExists($DriverID)) {
        $stmt = $this->con->prepare("UPDATE `lds_driver_info` SET `FacilityID` = ?, `Photo` = ? WHERE `DriverID` = ?");
        $stmt->bind_param("sbs", $FacilityID, $Photo, $DriverID);
        $result = $stmt->execute();
        $stmt->close();
        if($result) {
          return 0;
        } else {
          return 1;
        } 
      } else {
        return 2;
      }
    }

    //Method to fetch all drivers from database
    public function getAllDrivers() {
      $stmt = $this->con->prepare("SELECT * FROM lds_driver_info");
      $stmt->execute();
      $drivers = $stmt->get_result();
      $stmt->close();
      return $drivers;
    }

    //Method to check the driver already exist or not
    private function isDriverExists($DriverID) {
      $stmt = $this->con->prepare("SELECT DriverID from lds_driver_info WHERE DriverID = ?");
      $stmt->bind_param("s", $DriverID);
      $stmt->execute();
      $stmt->store_result();
      $num_rows = $stmt->num_rows;
      $stmt->close();
      return $num_rows > 0;
    }

    //Method to create or update a device info for driver
    public function driverDeviceUpdate($DriverID,$DeviceID,$PhoneNum,$PhoneIp,$AppVersion,$BarcodeMaxLength) {
      if (!$this->isDeviceExists($DriverID)) {
        //Insert the device info for the driver
        $stmt = $this->con->prepare("INSERT INTO `lds_device_info` (`DriverID`, `DeviceID`, `PhoneNum`, `PhoneIp`, `AppVersion`, `BarcodeMaxLength`) VALUES (?,?,?,?,?,?)");
        $stmt->bind_param("ssssss", $DriverID, $DeviceID, $PhoneNum, $PhoneIp, $AppVersion, $BarcodeMaxLength);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
          return 0;
        } else {
          return 1;
        }
      } else {
        //Update the device info for the driver
        $stmt = $this->con->prepare("UPDATE `lds_device_info` SET `DeviceID` = ? , `PhoneNum` = ? , `PhoneIp` = ?, `AppVersion` = ?, `BarcodeMaxLength` = ? WHERE `DriverID` =? ");
        $stmt->bind_param("ssssss", $DeviceID, $PhoneNum, $PhoneIp, $AppVersion, $BarcodeMaxLength, $DriverID);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
          return 2;
        } else {
          return 3;
        }
      }
    }

    //Method to check the device info already exist or not for driver
    private function isDeviceExists($DriverID) {
      $stmt = $this->con->prepare("SELECT DeviceID from lds_device_info WHERE DriverID = ?");
      $stmt->bind_param("s", $DriverID);
      $stmt->execute();
      $stmt->store_result();
      $num_rows = $stmt->num_rows;
      $stmt->close();
      return $num_rows > 0;
    }

    //Method to create or update a location info for driver
    public function driverLocationUpdate($DriverID,$Latitude,$Longitude,$GPSPrecision,$GPSFixDateTime,$GPSInterval) {
      if (!$this->isLocationExists($DriverID)) {
        //Insert the device info for the driver
        $stmt = $this->con->prepare("INSERT INTO `lds_location_info` (`DriverID`, `Latitude`, `Longitude`, `GPSPrecision`, `GPSInterval`) VALUES (?,?,?,?,?)");
        $stmt->bind_param("ssssi", $DriverID, $Latitude, $Longitude, $GPSPrecision, $GPSInterval);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
          return 0;
        } else {
          return 1;
        }
      } else {
        //Update the device info for the driver
        $stmt = $this->con->prepare("UPDATE `lds_location_info` SET `Latitude` = ?, `Longitude` = ?, `GPSPrecision` = ?, `GPSInterval` = ? WHERE `DriverID` = ? ");
        $stmt->bind_param("sssss", $Latitude, $Longitude, $GPSPrecision, $GPSInterval, $DriverID);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
          return 2;
        } else {
          return 3;
        }
      }
    }

    //Method to check the location info already exist or not for driver
    private function isLocationExists($DriverID) {
      $stmt = $this->con->prepare("SELECT DriverID from lds_location_info WHERE DriverID = ?");
      $stmt->bind_param("s", $DriverID);
      $stmt->execute();
      $stmt->store_result();
      $num_rows = $stmt->num_rows;
      $stmt->close();
      return $num_rows > 0;
    }

    //Method to generate a unique token every time
    private function generateGUID() {
      return md5(uniqid(rand(), true));
    }
	
	  //Method to fetch all manifestitems
    public function getAllManifestList() {
      $stmt = $this->con->prepare("SELECT * FROM lds_manifest");
      $stmt->execute();
      $manifest = $stmt->get_result();
      $stmt->close();
      return $manifest;
    }
	
  	//Method to fetch all manifestitem details with destination
  	public function getAllManifest($DriverID) {
      $stmt = $this->con->prepare("SELECT B.*,C.* FROM lds_driver_info A JOIN lds_manifest B ON B.DriverID=A.DriverID JOIN lds_manifest_destination C ON C.DriverID=A.DriverID WHERE A.DriverID = ? && C.Barcode = B.Barcode ");
  	  $stmt->bind_param("s", $DriverID);
      $stmt->execute();
      $manifest = $stmt->get_result();
      $stmt->close();
      return $manifest;
    }

    //Method to fetch all manifest details by barcode
    public function getAllManifestDetalis($Barcode) {
      $stmt = $this->con->prepare("SELECT A.*,B.* FROM lds_manifestdetails A JOIN  lds_manifestdetails_destination B  ON B.Barcode = A.Barcode WHERE A.Barcode = ? ");
      $stmt->bind_param("s", $Barcode);
      $stmt->execute();
      $manifest = $stmt->get_result();
      $stmt->close();
      return $manifest;
    }

    //Method to fetch all container deatails
    public function getAllContainerDetails() {
      $stmt = $this->con->prepare("SELECT * from lds_containers");
      $stmt->execute();
      $conatiner = $stmt->get_result();
      $stmt->close();
      return $conatiner;
    }

    //Method to update container details
    public function insertEventsInfo($DriverID, $Barcode, $CustomerID, $DoorTag, $EventActionValue, $EventISODateTime, $EventModifier, $EventType, $GroupGUID, $Location, $PieceKey, $SecurityGUID, $SignatureWKT, $TypedNameSignature, $EventDateTime, $EventID, $Latitude, $Longitude, $GPSFixDateTime, $DeviceID, $PhoneIp, $PhoneNum, $FacilityID ){
      $query = 'INSERT INTO lds_events (DriverID, Barcode, CustomerID, DoorTag, EventActionValue, EventISODateTime, EventModifier, EventType, GroupGUID, Location, PieceKey, SecurityGUID, SignatureWKT, TypedNameSignature, EventDateTime, EventID, Latitude, Longitude, GPSFixDateTime, DeviceID,PhoneIp, PhoneNum, FacilityID) VALUES ("'.$DriverID.'","'.$Barcode.'","'.$CustomerID.'","'.$DoorTag.'","'.$EventActionValue.'","'.$EventISODateTime.'","'.$EventModifier.'","'.$EventType.'","'.$GroupGUID.'","'.$Location.'","'.$PieceKey.'","'.$SecurityGUID.'","'.$SignatureWKT.'","'.$TypedNameSignature.'","'.$EventDateTime.'","'.$EventID.'","'.$Latitude.'","'.$Longitude.'","'.$GPSFixDateTime.'","'.$DeviceID.'","'.$PhoneIp.'","'.$PhoneNum.'","'.$FacilityID.'")';
      $stmt = $this->con->prepare($query);
      $result = $stmt->execute();
      $stmt->close();
      if ($stmt) {
        return 1;
      } else {
        return 2;
      }
    }

    //Method to fetch all CustomerConfig details
    public function customerConfig($CustomerID){
      $stmt = $this->con->prepare("SELECT Questionnaire FROM `lds_customer_config` WHERE `CustomerID` = ?");
      $stmt->bind_param("s", $CustomerID);
      $stmt->execute();
      $custConfig = $stmt->get_result()->fetch_assoc();
      $stmt->close();
      return $custConfig;
    }

    //Method to fetch all CustomerConfig's Attempts details
    public function customerAttempts(){
      $query = "SELECT * FROM lds_customer_attempts";
      $stmt = $this->con->prepare($query);
      $stmt->execute();
      $attempts = $stmt->get_result();
      $stmt->close();
      return $attempts;
    }

    //Method to fetch all CustomerConfig's DropLocation details
    public function customerDroplocations(){
      $query = "SELECT * FROM lds_customer_droplocations";
      $stmt = $this->con->prepare($query);
      $stmt->execute();
      $droplocations = $stmt->get_result();
      $stmt->close();
      return $droplocations;
    }

    //Method to fetch all CustomerConfig's Questionnaire details
    public function customerQuestionnaire(){
      $query = "SELECT * FROM lds_customer_questionnaire";
      $stmt = $this->con->prepare($query);
      $stmt->execute();
      $questionnaire= $stmt->get_result();
      $stmt->close();
      return $questionnaire;
    }
  }
?>