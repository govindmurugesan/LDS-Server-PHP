<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require dirname(__FILE__) . '/../vendor/autoload.php';

$app = new \Slim\App;
require_once dirname(__FILE__) . '/../app/api/logistics.php';

$app->run();